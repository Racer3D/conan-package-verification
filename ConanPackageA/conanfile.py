from conans import ConanFile

class ConanPackageAConan(ConanFile):
    name = "ConanPackageA"
    version = "1.0"
    settings = ()

    def package(self):
        self.copy("*", src="./package", dst="", keep_path=True)
