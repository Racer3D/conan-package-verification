from conans import ConanFile

class ConanPackageCConan(ConanFile):
    name = "ConanPackageC"
    version = "1.0"
    settings = ()

    def package(self):
        self.copy("*", src="./package", dst="", keep_path=True)