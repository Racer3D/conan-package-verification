from conans import ConanFile
import os

class ConanPackageBConan(ConanFile):
    name = "ConanPackageB"
    version = "1.0"
    settings = ()

    def requirements(self):
        self.requires("ConanPackageA/1.0@{}/stable".format(os.environ['CONAN_USER']))

    def package(self):
        self.copy("*", src="./package", dst="", keep_path=True)